package com.mindgame.tutorialfor.business.management2018;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Ajay on 3/19/2018.
 */

class Menus_singleton {
    private static final Menus_singleton ourInstance = new Menus_singleton();
    public JSONArray menus_json = new JSONArray();
    public int topic_index;
    public String ASSET_STRING = "file:///android_asset/";


    static Menus_singleton getInstance() {



        return ourInstance;
    }

    private Menus_singleton() {



    }
    /* Returns the String array list of menu titles for the given string*/
public ArrayList<String> get_menu_titles(int index){
    
    ArrayList<String> menu_title_array = new ArrayList<>();
    JSONArray jarray2 = new JSONArray();
    
    try {


        JSONObject jb = (JSONObject) menus_json.get(index);
        String topic = jb.getString("topic");

        jarray2 = jb.getJSONArray("menus");

        for (int i = 0; i < jarray2.length(); i++) {
            menu_title_array.add(jarray2.getString(i));
        }
        


    }
    catch (Exception e){
        e.printStackTrace();
    }

    return menu_title_array;
}

    /* Returns the String array list of menu titles for the given string*/
public String get_menu_file(int pos){

    String menu_title_file = "";
    JSONArray jarray2 = new JSONArray();

    try {


        JSONObject jb = (JSONObject) menus_json.get(topic_index);
        String topic = jb.getString("topic");
        jarray2 = jb.getJSONArray("files");

        menu_title_file = ASSET_STRING + topic +"/" + jarray2.getString(pos);

    }
    catch (Exception e){
        e.printStackTrace();
    }

    return menu_title_file;
}

/*Return the Topic*/
public String get_topic(int pos){
    String menu_topic = "";

    try {
        JSONObject jb = (JSONObject) menus_json.get(pos);
        menu_topic = jb.getString("topic");

    }

    catch (Exception e){
        e.printStackTrace();
    }

    return menu_topic;
}

}
