package com.mindgame.tutorialfor.business.management2018;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class MainActivity extends Activity {
    RecyclerView recyclerView;

    String val;
    TextView heading;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13;


    Boolean connected;

    Menus_singleton Mnu = Menus_singleton.getInstance();
    TextView terms_privacy, title_page;


    ScrollView scrollView;
    public static String []
            array_cps_itemsm=Html_List.array_cps_items
            ,array_crm_items=Html_List.array_crm_items,
             array_fa_item=Html_List.array_fa_item,
             array_ipp_items=Html_List.array_ipp_items
            ,array_km_items=Html_List.array_km_items,
             array_me_items=Html_List.array_me_items;

    NetworkStatusCheck NC= NetworkStatusCheck.getInstance();
    singleton_images sc=singleton_images.getInstance();
    String app_status = sc.app_mode();
    int index;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Bundle b=getIntent().getExtras();
//        val=b.getString("flag");

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);
        Intent i = getIntent();
        index = i.getIntExtra("index",0);




        initViews();
        if (app_status.equals("TEST")) {

            recyclerView.setVisibility(View.INVISIBLE);

        }
        if (app_status.equals("PROD"))

        {

                //save index in the singleton class
                Mnu.topic_index = index;
                recyclerView.setHasFixedSize(true);

                title_page.setText(Mnu.get_topic(index));
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
                recyclerView.setLayoutManager(layoutManager);
                DataAdapter adapter = new DataAdapter(getApplicationContext(), Mnu.get_menu_titles(index));
//                Log.d("index:", index.);
                recyclerView.setAdapter(adapter);

              }
    }



    private void initViews() {
        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        array_cps_itemsm=getResources().getStringArray(R.array.array_cps_items);
        array_crm_items=getResources().getStringArray(R.array.array_crm_items);
        array_fa_item=getResources().getStringArray(R.array.array_fa_items);
        array_ipp_items=getResources().getStringArray(R.array.array_ipp_items);
        array_km_items=getResources().getStringArray(R.array.array_km_items);
        array_me_items=getResources().getStringArray(R.array.array_me_items);
        title_page = (TextView) findViewById(R.id.heading_text);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(MainActivity.this,Menu_Page.class);
        startActivity(i);
    }
}
