package com.mindgame.tutorialfor.business.management2018;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;

public class Menu_Page extends AppCompatActivity {
    ImageView one,two,three,four,five,six,seven,eight;
    TextView txt_1,txt_2,txt_3,txt_4;
    ScrollView scrollView;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    singleton_images sc=singleton_images.getInstance();
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page = "ca-app-pub-4951445087103663/3087467386";
    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 = "ca-app-pub-4951445087103663/3087467386";
    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();
    Menu_html_sigleton MS = new Menu_html_sigleton();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);


        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);

        final AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);
        String app_status = sc.app_mode();
        initViews();
        //loading data




        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.putExtra("index",0);
                    startActivity(i);
            }
        });


        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("index",1);
                startActivity(i);

            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("index",2);
                startActivity(i);

            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("index",3);
                startActivity(i);



            }
        });


        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("index",4);
                startActivity(i);



            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("index",5);
                startActivity(i);

            }
        });

    }
    public void initViews(){
        one=(ImageView) findViewById(R.id.one);
        two=(ImageView) findViewById(R.id.two);
        three=(ImageView) findViewById(R.id.three);
        four=(ImageView) findViewById(R.id.four);
        five=(ImageView)findViewById(R.id.five);
        six=(ImageView)findViewById(R.id.six);

        scrollView=(ScrollView)findViewById(R.id.scrollView);

        txt_1=(TextView)findViewById(R.id.txt_1);
        txt_2=(TextView)findViewById(R.id.txt_2);
        txt_3=(TextView)findViewById(R.id.txt_3);
        txt_4=(TextView)findViewById(R.id.txt_4);




    }
}
