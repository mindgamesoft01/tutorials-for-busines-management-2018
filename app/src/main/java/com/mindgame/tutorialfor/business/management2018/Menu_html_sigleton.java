package com.mindgame.tutorialfor.business.management2018;

/**
 * Created by Ajay on 3/16/2018.
 */

class Menu_html_sigleton {
    int cat;
    String [] menu_title;
    String [] menu_html_path;

    public int getCat() {
        return cat;
    }

    public void setCat(int cat) {
        this.cat = cat;
    }

    public String[] getMenu_title() {
        return menu_title;
    }

    public void setMenu_title(String[] menu_title) {
        this.menu_title = menu_title;
    }

    public String[] getMenu_html_path() {
        return menu_html_path;
    }

    public void setMenu_html_path(String[] menu_html_path) {
        this.menu_html_path = menu_html_path;
    }




}
